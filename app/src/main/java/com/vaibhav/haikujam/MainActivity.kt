package com.vaibhav.haikujam

import android.animation.AnimatorSet
import android.animation.ObjectAnimator
import android.os.Bundle
import android.view.View
import android.view.ViewTreeObserver
import androidx.appcompat.app.AppCompatActivity
import androidx.core.animation.addListener
import androidx.core.content.ContextCompat
import kotlinx.android.synthetic.main.activity_main.*

/**
 * @author Vaibhav Bhandula on 2019-05-01.
 */
class MainActivity : AppCompatActivity() {

    private var phase = 0
    private var img1X = 0.0f
    private var img2X = 0.0f
    private var img3X = 0.0f
    private var img1Y = 0.0f
    private var img2Y = 0.0f
    private var img3Y = 0.0f
    private var view1X = 0.0f
    private var view2X = 0.0f
    private var view3X = 0.0f
    private var view1Y = 0.0f
    private var view2Y = 0.0f
    private var view3Y = 0.0f

    private val redColor by lazy {
        ContextCompat.getColor(this, R.color.red)
    }
    private val lightRedColor by lazy {
        ContextCompat.getColor(this, R.color.light_red)
    }
    private val orangeColor by lazy {
        ContextCompat.getColor(this, R.color.orange)
    }
    private val darkGreyColor by lazy {
        ContextCompat.getColor(this, android.R.color.darker_gray)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initWidgets()
    }

    private fun initWidgets() {
        card_child_layout?.visibility = View.GONE
        images_layout?.visibility = View.GONE
        bt_next?.setOnClickListener { performNextPhase() }
    }

    private fun performNextPhase() {
        when (phase) {
            0 -> {
                //convert card to circle
                convertCardToCircle()
                phase = 1
            }
            1 -> {
                //animate image 2
                animateImage2()
                phase = 2
            }
            2 -> {
                //animate image 1
                animateImage1()
                phase = 3
            }
            3 -> {
                //animate image 3
                animateImage3()
                phase = 4
            }
            4 -> {
                //convert card back to square
                convertCircleToCard()
                phase = 0
            }
        }
    }

    private fun setDefaults() {
        images_layout?.visibility = View.GONE
        card_child_layout?.visibility = View.GONE
        view_1?.setBackgroundColor(darkGreyColor)
        view_2?.setBackgroundColor(darkGreyColor)
        view_3?.setBackgroundColor(darkGreyColor)
        image_1?.x = img1X
        image_2?.x = img2X
        image_3?.x = img3X
        image_1?.y = img1Y
        image_2?.y = img2Y
        image_3?.y = img3Y
        image_1?.alpha = 1.0f
        image_2?.alpha = 1.0f
        image_3?.alpha = 1.0f
    }

    private fun convertCardToCircle() {
        val currentRadius = Utils.convertDpToPixel(this, 20)
        val finalRadius = Utils.convertDpToPixel(this, 150)
        val objectAnimator = ObjectAnimator.ofFloat(main_card, "radius", currentRadius, finalRadius)
        objectAnimator.duration = 500
        objectAnimator.addListener({
            main_card?.cardElevation = 0.toFloat()
            images_layout?.visibility = View.VISIBLE
            card_child_layout?.visibility = View.VISIBLE
            calculateViewsXAndY()
        })
        objectAnimator.start()
    }

    private fun convertCircleToCard() {
        setDefaults()
        val currentRadius = Utils.convertDpToPixel(this, 150)
        val finalRadius = Utils.convertDpToPixel(this, 20)
        val objectAnimator = ObjectAnimator.ofFloat(main_card, "radius", currentRadius, finalRadius)
        objectAnimator.duration = 500
        objectAnimator.addListener({
            main_card?.cardElevation = Utils.convertDpToPixel(this, 2)
        })
        objectAnimator.start()
    }

    private fun calculateViewsXAndY() {
        view_3?.viewTreeObserver?.addOnGlobalLayoutListener(object : ViewTreeObserver.OnGlobalLayoutListener {
            override fun onGlobalLayout() {
                val view1LocArray = IntArray(2)
                val view2LocArray = IntArray(2)
                val view3LocArray = IntArray(2)
                view_1.getLocationInWindow(view1LocArray)
                view_2.getLocationInWindow(view2LocArray)
                view_3.getLocationInWindow(view3LocArray)
                view1X = view1LocArray[0].toFloat()
                view2X = view2LocArray[0].toFloat()
                view3X = view3LocArray[0].toFloat()
                view1Y = view1LocArray[1].toFloat()
                view2Y = view2LocArray[1].toFloat()
                view3Y = view3LocArray[1].toFloat()
                img1X = image_1.x
                img2X = image_2.x
                img3X = image_3.x
                img1Y = image_1.y
                img2Y = image_2.y
                img3Y = image_3.y
                view_3?.viewTreeObserver?.removeOnGlobalLayoutListener(this)
            }
        })
    }

    private fun animateImage2() {
        view_1.post {
            val xAnimator = ObjectAnimator.ofFloat(image_2, "x", img2X, view1X)
            val yAnimator = ObjectAnimator.ofFloat(image_2, "y", img2Y, view1Y)
            val alphaAnimator = ObjectAnimator.ofFloat(image_2, "alpha", 1.0f, 0.0f)

            val set = AnimatorSet()
            set.playTogether(xAnimator, yAnimator, alphaAnimator)
            set.duration = 500
            set.addListener({
                view_1?.setBackgroundColor(redColor)
                image_2?.clearAnimation()
            })
            set.start()
        }
    }

    private fun animateImage1() {
        view_2.post {
            val xAnimator = ObjectAnimator.ofFloat(image_1, "x", img1X, view2X)
            val yAnimator = ObjectAnimator.ofFloat(image_1, "y", img1Y, view2Y)
            val alphaAnimator = ObjectAnimator.ofFloat(image_1, "alpha", 1.0f, 0.0f)

            val set = AnimatorSet()
            set.playTogether(xAnimator, yAnimator, alphaAnimator)
            set.duration = 500
            set.addListener({
                view_2?.setBackgroundColor(lightRedColor)
                image_1?.clearAnimation()
            })
            set.start()
        }
    }

    private fun animateImage3() {
        view_3.post {
            val xAnimator = ObjectAnimator.ofFloat(image_3, "x", img3X, view3X)
            val yAnimator = ObjectAnimator.ofFloat(image_3, "y", img3Y, view3Y)
            val alphaAnimator = ObjectAnimator.ofFloat(image_3, "alpha", 1.0f, 0.0f)

            val set = AnimatorSet()
            set.playTogether(xAnimator, yAnimator, alphaAnimator)
            set.duration = 500
            set.addListener({
                view_3?.setBackgroundColor(orangeColor)
                image_3?.clearAnimation()
            })
            set.start()
        }
    }

}
