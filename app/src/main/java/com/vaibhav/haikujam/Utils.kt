package com.vaibhav.haikujam

import android.content.Context
import android.util.DisplayMetrics

/**
 * @author Vaibhav Bhandula on 2019-05-01.
 */
object Utils {

    fun convertDpToPixel(context: Context?, dp: Int): Float {
        if (context == null) {
            return 0.0f
        }
        val resources = context.resources
        val metrics = resources.displayMetrics
        return dp * (metrics.densityDpi.toFloat() / DisplayMetrics.DENSITY_DEFAULT)
    }
}
